const express = require('express');
const bodyParser = require('body-parser');
const {sequelize, Profile, Contract, Job} = require('./model')
const {getProfile} = require('./middleware/getProfile')
const app = express();
app.use(bodyParser.json());
app.set('sequelize', sequelize)
app.set('models', sequelize.models)
const { Op, QueryTypes } = require("sequelize");

/**
 * @returns contract by id
 */
app.get('/contracts/:id',getProfile ,async (req, res) => {
    const {id} = req.params
    const userId = req.get('profile_id')
    const contract = await Contract.findOne({where: {id}})
    if (!contract) return res.status(404).end()
    if (contract.ClientId != userId && contract.ContractorId != userId) return res.status(403).end() // forbidden
    res.json(contract)
})

app.get('/contracts',getProfile ,async (req, res) => {
    const userId = req.profile.id
    const contracts = await Contract.findAll({where: {[Op.or]: {ContractorId: userId, ClientId: userId}, status: {[Op.ne]: 'terminated'}}})
    res.json(contracts)
})

app.get('/jobs/unpaid',getProfile ,async (req, res) => {
    const userId = req.profile.id
    const contracts = await Contract.findAll({where: {[Op.or]: {ContractorId: userId, ClientId: userId}, status: ['in_progress']}})
    if (contracts.length === 0) return res.json([])
    const contractIds = contracts.map(c => c.id)
    const jobs = await Job.findAll({where: {ContractId: contractIds, paid: {[Op.is]: null} }}) // it could be improved to consier paid == false
    res.json(jobs)
})

app.post('/jobs/:job_id/pay',getProfile ,async (req, res) =>{
    const client = req.profile
    const job = await Job.findOne({where: {id: req.params.job_id}})
    if (!job) return res.status(404).end()
    const contract = await Contract.findOne({where: {id: job.ContractId}})
    const contractor = await Profile.findOne({where: {id: contract.ContractorId }}) 

    if (client.type !== 'client') return res.status(422).end('Only client profiles can pay a job')
    if (contract.ClientId != req.profile.id) return res.status(403).end() // forbidden
    if (job.paid) return res.status(422).end('Cant pay, job is already paid')
    if (job.price > client.balance) return res.status(422).end('Cant pay, there is not enough balance!')

    const newBalanceClient = client.balance - job.price
    const newBalanceContractor = contractor.balance + job.price

    const trx = await sequelize.transaction();

    try {
        // update client balance
        await Profile.update(
            { balance: newBalanceClient }, 
            { where: { id: client.id } }, 
            { transaction: trx })

        // update contractor balance
        await Profile.update(
            { balance: newBalanceContractor }, 
            { where: { id: contractor.id } },
            { transaction: trx })

        // update job status
        await Job.update(
            { paid: true, paymentDate: new Date() }, 
            { where: {id: job.id } },
            { transaction: trx })

            trx.commit()
            res.status(200).end()
        } catch (e) {
            trx.rollback()
            console.error(e)
            res.status(422).end()
        }

        // TODO: update contract status? Maybe only if all jobs are finished!
})

app.post('/balances/deposit/:userId', getProfile, async (req, res) => {
    const amount = req.body.amount

    // TODO: Shouldn't user id came always from authentication?
    const userId = req.params.userId

    if (!amount || amount == 0) return res.status(400).end('The amount is invalid')

    const client = await Profile.findOne({where: {id: userId}})
    if (client.type !== 'client') return res.status(422).end('Deposits can be made only in a client profile')

    const contracts = await Contract.findAll({where: {ClientId: client.id}})
    if (contracts.length === 0) return res.status(422).end('Cant deposit, the amount exceeds 25% of total due')
    const contractIds = contracts.map(c => c.id)
    let totalDue = await Job.sum('price', {where: {ContractId: contractIds, paid: {[Op.is]: null} }} )
    console.log(totalDue)

    const maxDeposit = totalDue * 0.25
    if (amount > maxDeposit) return res.status(422).end('Cant deposit, the amount exceeds 25% of total due')

    const newBalanceClient = client.balance + amount
    // update client balance
    await Profile.update(
        { balance: newBalanceClient }, 
        { where: { id: client.id } })

    res.status(200).end()
})

app.get('/admin/best-profession', getProfile, async (req, res) => {
    const startDate = req.query.start
    const endDate = req.query.end

    if (!startDate || !endDate) return res.status(400).end('Please inform the parameters "start" and "end"')
    
    // TODO: I don't have enough knowledge of sequelize, so I did it with native query
    const result = await sequelize.query(`
      select p.profession, sum(j.price) as total
        from Jobs j
        join Contracts c
          on j.ContractId = c.id
        join Profiles p 
          on c.ContractorId = p.id
       where j.paid = 1
         and j.paymentDate between :startDate and :endDate
       group by p.profession
       order by total desc
       limit 1`,
       { 
         replacements: { startDate, endDate },
         type: QueryTypes.SELECT
       })
    
    if (result.length > 0) res.json(result[0])
    else res.json(null)
})

app.get('/admin/best-clients', getProfile, async (req, res) => {
    const startDate = req.query.start
    const endDate = req.query.end
    const limit = req.query.limit || 2

    if (!startDate || !endDate) return res.status(400).end('Please inform the parameters "start" and "end"')

    // TODO: I don't have enough knowledge of sequelize, so I did it with native query
    const result = await sequelize.query(`
      select p.id, p.firstName, p.lastName, sum(j.price) as paid
        from Jobs j
        join Contracts c
          on j.ContractId = c.id
        join Profiles p 
          on c.ClientId = p.id
       where j.paid = 1  
         and paymentDate between :startDate and :endDate
       group by p.profession
       order by paid desc
       limit :limit`,
       { 
         replacements: { startDate, endDate, limit },
         type: QueryTypes.SELECT
       })

    const resultsFormated = result.map(item => {
        const formated = {id: item.id, fullName: `${item.firstName} ${item.lastName}`, paid: item.paid}
        return formated 
    })
    res.json(resultsFormated)
})

module.exports = app;
